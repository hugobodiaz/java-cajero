import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

enum tipo_de_operacion{
    INGRESAR,
    RETIRAR
}

abstract class Command{
    String help_print_scrn;
    String name;
    public void execute(){};

}

class Command_help extends Command {
    Command_help(){
        help_print_scrn = "help - enseña los comandos disponibles";
        name = "help";
    };

    @Override
    public void execute() {
        Main.print_help();
        super.execute();
    }
}

class Command_imposicion extends Command {
    Command_imposicion(){
        help_print_scrn = "imposición - permite depositar dinero en la cuenta deseada";
        name = "imposicion";
    };

    @Override
    public void execute() {
        Main.Mover_dinero(tipo_de_operacion.INGRESAR);
        super.execute();
    }
}

class Command_reintegro extends Command {
    Command_reintegro(){
        help_print_scrn = "reintegro - permite sacar dinero de su cuenta deseada";
        name = "reintegro";
    };

    @Override
    public void execute() {
        Main.Mover_dinero(tipo_de_operacion.RETIRAR);
        super.execute();
    }
}

class Command_transferencia extends Command {
    Command_transferencia(){
        help_print_scrn = "transferencia - permite transferir dinero de la cuenta que quiera a otras";
        name = "transferencia";
    };

    @Override
    public void execute() {
        Main.transferencia();
        super.execute();
    }
}

class Command_ver_movimientos extends Command {
    Command_ver_movimientos(){
        help_print_scrn = "ver movimientos - enseña una lista con los ultimos 50 movimientos realizados en la cuenta";
        name = "ver movimientos";
    };

    @Override
    public void execute() {
        Main.ver_movimientos();
        super.execute();
    }
}

class Command_cambiar_pin extends Command {
    Command_cambiar_pin(){
        help_print_scrn = "cambiar pin - permite cambiar el pin de acceso a esta cuenta";
        name = "cambiar pin";
    };

    @Override
    public void execute() {
        Main.cambiar_pin();
        super.execute();
    }
}

class Command_ver_saldo extends Command {
    Command_ver_saldo(){
        help_print_scrn = "ver saldo - enseña el saldo de la cuenta";
        name = "ver saldo";
    };

    @Override
    public void execute() {
        Main.ver_saldo();
        super.execute();
    }
}

public class Main {

    static DBcontroller DBcon;
    static boolean salir = false;
    static User user;
    static List<Cuenta> cuentas = new ArrayList<Cuenta>();

    static List<Command> comandos = new ArrayList<Command>();

    public static int elegir_cuenta()
    {
        int cuenta = -1;
        switch (cuentas.size()) {
            case 0:
                System.out.println("Parece que no tienes ninguna cuenta a tu nombre!");
                
                break;
            case 1:
                cuenta = 0;
                System.out.println("Operando sobre: " + cuentas.get(0).titulo + " - " + cuentas.get(0).numero_de_cuenta);

                break;
            default:
                System.out.println("Elige la cuenta sobre la que quieres operar:");
                int iterator=0;
                for (Cuenta cuenta_i : cuentas) {

                    String out = iterator + " - " + cuenta_i.titulo + " - " + cuenta_i.numero_de_cuenta;
                    System.out.println(out);

                    iterator++;
                }
                Console cnsl = System.console();

                cuenta = Utilities.readInt(cnsl, "Eso no es un número, por favor escriba un número");
                while (cuenta > cuentas.size()-1 || cuenta<0) {
                    System.out.println("Número de cuenta no valido, vuelva a intentar");
                    cuenta = Utilities.readInt(cnsl, "Eso no es un número, por favor escriba un número");
                }
        }
        return cuenta;
    }

    public static void obtener_cuentas()
    {

        if(user ==null){
            System.out.println("No hay ningún usuario logeado");
        }
        else{
            try{
                DBcon.Connect();

                //esta query nos consigue las cuentas que pertenecen al usuario
                String query = "SELECT uc.Cuentas_idCuentas, c.`numero de cuenta`, c.`Título de la cuenta` "+
                                "FROM mydb.usuarios_has_cuentas uc left join cuentas c on(c.idCuentas=uc.Cuentas_idCuentas) "+
                                "where Usuarios_idUsuarios=" + user.ID + ";";
                ResultSet res =  DBcon.ExecuteQuery(query);

                while (res.next()) {
                    Cuenta c = new Cuenta();
                    c.id=res.getInt("Cuentas_idCuentas");
                    c.numero_de_cuenta =res.getString("numero de cuenta");
                    c.titulo= res.getString("Título de la cuenta");
                    cuentas.add(c);
                }
                DBcon.CloseQuery(res);
                DBcon.Disconnect();
            }
            catch(SQLException ex)
            {
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }

        }

    }

    public static Double obtener_saldo(Cuenta cuenta)
    {
        Double ret = 0.0;
        try {
            DBcon.Connect();

            //esta query nos consigue los movimientos de la cuenta
            String query = "SELECT SUM(Cantidad) FROM mydb.movimientos  where Cuentas_idCuentas = "+cuenta.id+";";
            ResultSet res =  DBcon.ExecuteQuery(query);

            //recorremos todos los registros y enseñamos la informacion al usuario
            while (res.next()) {
                ret =res.getDouble("SUM(Cantidad)");

            }
            DBcon.CloseQuery(res);
            DBcon.Disconnect();
        } catch (Exception e) {
            //TODO: handle exception
        }

        return ret;
    }

    public static boolean tiene_dinero_para_operar(Double cantidad, Cuenta cuenta)
    {
        Double saldo = obtener_saldo(cuenta);

        if(saldo > cantidad)
            return true;
        else
            return false;

    }
    

    public static void Mover_dinero(tipo_de_operacion t){


        String operacion="";
        switch (t) {
            case INGRESAR:
            System.out.println("---Ingresar dinero---");
            operacion = "IMPOSICIÓN";
                break;
            case RETIRAR:
            System.out.println("---Retirar dinero---");
            operacion = "REINTEGRO";
                break;
        
            default:
            //salimos de la función si llega aquí, no deberia pasar nunca
                return;
        }

        Cuenta c =cuentas.get(elegir_cuenta());

        switch (t) {
            case INGRESAR:
            System.out.println("indique la cantidad de dinero que quiere ingresar:");
            break;
            case RETIRAR:
            System.out.println("indique la cantidad de dinero que quiere retirar:");
            break;
        
            default:
            //salimos de la función si llega aquí, no deberia pasar nunca
                return;
        }
                
        Console cnsl = System.console();
        Double cantidad;
        cantidad = Utilities.readDouble(cnsl, "Eso no es un número, por favor escriba un número");
        cantidad = Math.abs(cantidad);
        
        //actuaremos de una manera u otra segun la variable "t", que define el tipo de operación
        if(t==tipo_de_operacion.RETIRAR)
        {
            //miramos si es posible retirar esa cantidad de dinero de la cuenta
            while(!tiene_dinero_para_operar(cantidad, c))
            {
                System.out.println("No dispone de esa cantidad para transferir!,vuelva a intentarlo...");
                cantidad = Utilities.readDouble(cnsl, "Eso no es un número, por favor escriba un número");
                cantidad = Math.abs(cantidad);
            }

            cantidad = -cantidad;
        }

        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());


        String query = "INSERT INTO `mydb`.`movimientos`"+
        "(`Cuentas_idCuentas`, `Destinatario`, `Cantidad`, `Fecha_inicio`, `Fecha_final`, `Razón`) "+
        "VALUES ("+c.id+",'"+user.nombre+"',"+cantidad+",'"+date.toString()+"','"+date.toString()+"','"+operacion+"');";

        //System.out.println(query);

        try {
            DBcon.Connect();
            DBcon.UpdateTable(query);
            DBcon.Disconnect();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        
    }

    public static void transferencia(){
        System.out.println("---Transferencia---");
        Cuenta c =cuentas.get(elegir_cuenta());

        Console cnsl = System.console();

        //preguntamos los datos necesarios al usuario para la query
        System.out.println("Escriba el número de cuenta a la que quiere transferir dinero:");
        String dest = cnsl.readLine();

        System.out.println("Escriba la cantidad de dinero que quiere mandar:");
        Double cant = Utilities.readDouble(cnsl, "Eso no es un número, por favor escriba un número");
        while(!tiene_dinero_para_operar(cant, c))
        {
            System.out.println("No dispone de esa cantidad para transferir!,vuelva a intentarlo...");
            cant = Utilities.readDouble(cnsl, "Eso no es un número, por favor escriba un número");
            return;
        }

        System.out.println("Escriba la razón de esta transferencia (max 30 car.):");
        String razn = cnsl.readLine();

        while(razn.length() >=30)
        {
            System.out.println("Es un texto demasiado largo, pruebe con uno mas corto:");
            razn = cnsl.readLine();
        }
        
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());

        try {
            DBcon.Connect();

            //generamos la query y ponemos la información en la base de datos
            String insert = "INSERT INTO `mydb`.`movimientos`"+
            "(`Cuentas_idCuentas`, `Destinatario`, `Cantidad`, `Fecha_inicio`, `Fecha_final`, `Razón`) "+
            "VALUES ("+c.id+",'"+dest+"',"+cant+",'"+date.toString()+"','"+date.toString()+"','"+ razn +"');";;

            DBcon.UpdateTable(insert);

            String query = "SELECT * from mydb.cuentas WHERE `numero de cuenta` = '"+ dest+"';";
            ResultSet cuenta_destino = DBcon.ExecuteQuery(query);
            
            //nos aseguramos de que si las dos cuentas son del banco la transferencia es grabada de ambos lados
            while(cuenta_destino.next())
            {
                String insert1 = "INSERT INTO `mydb`.`movimientos`"+
                "(`Cuentas_idCuentas`, `Destinatario`, `Cantidad`, `Fecha_inicio`, `Fecha_final`, `Razón`) "+
                "VALUES ("+cuenta_destino.getString("idCuentas")+",'"+c.numero_de_cuenta+"',-"+cant+",'"+date.toString()+"','"+date.toString()+"','"+ razn +"');";;
                
                DBcon.UpdateTable(insert1);
            }
            DBcon.CloseQuery(cuenta_destino);

            DBcon.Disconnect();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());        }


    }


    public static void ver_saldo(){
        System.out.println("---Ver saldo---");
        Cuenta c =cuentas.get(elegir_cuenta());

        Double saldo = obtener_saldo(c);
        
        System.out.println("El saldo de la cuenta '"+c.titulo+"' es: "+saldo+"");
        

    }

    public static void ver_movimientos(){
        System.out.println("---Ver movimientos---");
        Cuenta c =cuentas.get(elegir_cuenta());

        try {
            DBcon.Connect();

            //esta query nos consigue los movimientos de la cuenta
            String query = "SELECT * FROM mydb.movimientos  where Cuentas_idCuentas = "+ c.id +" order by `Fecha_final` asc limit 50;";
            ResultSet res =  DBcon.ExecuteQuery(query);

            //recorremos todos los registros y enseñamos la informacion al usuario
            while (res.next()) {

                System.out.println(
                "Destinatario: "+res.getString("Destinatario")
                +" - Cantidad: "+res.getString("Cantidad")
                +" - Razón: "+res.getString("Razón")
                + "- Fecha: "+res.getDate("Fecha_final").toString() );

            }
            DBcon.CloseQuery(res);
            DBcon.Disconnect();
        } catch (Exception e) {
            //TODO: handle exception
        }



    }

    public static void cambiar_pin(){
        System.out.println("---Cambiar PIN---");
        Console cnsl = System.console();

        //primero preguntamos al usuario el PIN actual por seguridad
        System.out.println("Introduzca el PIN actual:");
        String in = cnsl.readLine();

        //creamos un loop para que tenga tantas oportunidades como sea necesario para ponerlo
        //si no lo consigue poner puede ir al menu anterior poniendo "salir"
        boolean correct_password = false;
        boolean exit_loop = false;
        while (exit_loop==false)
        {
            if(in.equals("salir"))
            {
                exit_loop = true;
                break;
            }
            if(in.equals(user.PIN))
            {
                correct_password=true;
                exit_loop = true;
                break;
            }

            System.out.println("PIN equivocado, vuelva a intentarlo. Para salir escriba 'salir' ");
            in = cnsl.readLine();
        }
        
        if(correct_password)
        {
            //preguntamos por la contraseña doble porque es el estandar hoy en dia
            String pin1;
            String pin2;
            System.out.println("Introduzca el nuevo PIN:");
            pin1= cnsl.readLine();
            System.out.println("Vuelva a introducir el nuevo PIN:");
            pin2= cnsl.readLine();

            if(pin1.equals(pin2))
            {
                try {
                    //hacemos la query y cambiamos el PIN en la base de datos
                    DBcon.Connect();
                    user.PIN = pin1;

                    String update = "UPDATE mydb.usuarios SET Contraseña ='" + pin1 + "' where idUsuarios = "+user.ID;

                    DBcon.UpdateTable(update);
                    DBcon.Disconnect();
                } catch (SQLException ex) {
                    System.out.println("SQLException: " + ex.getMessage());
                    System.out.println("SQLState: " + ex.getSQLState());
                    System.out.println("VendorError: " + ex.getErrorCode());                }
            }

        }
        

    }

    public static void print_help()
    {
        System.out.println("Los comandos disponibles son:");
        //recogemos el texto asignado a cada comando y hacemos un print por cada uno
        for (Command command : comandos) {
            System.out.println(command.help_print_scrn);
        }


    }

    public static boolean Login()
    {
        Console cnsl = System.console();
        boolean has_logged_in = false;
        boolean exit_loop = false;
        while(!exit_loop)
        {
            //pedimos datos
            System.out.println("Introduzca su nombre:");
            String login_user = cnsl.readLine();
            if(login_user.equals("salir"))
            {
                exit_loop=true;
                System.out.println("Saliendo del cajero...");
                break;
            }

            System.out.println("Introduzca su contraseña:");
            String login_pin = cnsl.readLine();

            try 
            {
                //entramos a la database a preguntar si son correctos
                DBcon.Connect();                
                String query = "SELECT COUNT(*) FROM mydb.usuarios where Nombre='" + login_user + "' and Contraseña='"+login_pin+"';";
                ResultSet res =  DBcon.ExecuteQuery(query);

                //si lo son, hacemos el login de usuario consiguiendo el registro
                int number_found = 0;
                while(res.next())
                {
                    number_found = res.getInt("COUNT(*)");
                }
                DBcon.CloseQuery(res);

                if( number_found == 1 ){

                    has_logged_in=true;
                    exit_loop = true;

                    query = "SELECT * FROM mydb.usuarios where Nombre='" + login_user + "' and Contraseña='"+login_pin+"';";
                    ResultSet res1 =  DBcon.ExecuteQuery(query);
                    while(res1.next())
                    {
                        user = new User();
                        user.ID =(Integer) res1.getInt("idUsuarios");
                        user.nombre = (String) res1.getString("Nombre");
                        user.apellido_1 = (String) res1.getString("Apellido 1");
                        user.apellido_2 = (String) res1.getString("Apellido 2");
                        user.PIN = (String) res1.getString("Contraseña");
                    }
                    
                    System.out.println("Hola de nuevo, " + user.nombre + " " + user.apellido_1);

                    DBcon.CloseQuery(res1);

                    obtener_cuentas();

                }
                else
                {
                    System.out.println("Usuario no reconocido, prueba otra vez :(");
                }


                DBcon.Disconnect();
            }
            catch(SQLException ex)
            {
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }

        }            
        return has_logged_in;

    }

    public static void main(String[] args) {
        
        //iniciamos el database controller
        DBcon = new DBcontroller();

        System.out.println("Bienvenido al banco!");

        //añadimos los comandos a la lista para accederlos facilmente
        comandos.add(new Command_help());
        comandos.add(new Command_imposicion());
        comandos.add(new Command_reintegro());
        comandos.add(new Command_ver_movimientos());
        comandos.add(new Command_ver_saldo());
        comandos.add(new Command_transferencia());
        comandos.add(new Command_cambiar_pin());

        //intentamos hacer que el usuario haga login
        if(Login()){

            Console cnsl = System.console();

            //entramos al loop de acciones que puede hacer en el cajero
            while(!salir)
            {        
                //recogemos el comando
                System.out.println("Introduzca una acción:");
                String comando = cnsl.readLine();

                if(comando.equals("salir"))
                {
                    break;
                }

                boolean valid_command = false;

                //lo comparamos con la lista que tenemos
                for (Command temp : comandos) {
                    if(temp.name.equals(comando))
                    {
                        //si alguno coincide lo ejecutamos
                        temp.execute();
                        valid_command=true;
                        break;
                    }
                }
                if(!valid_command)
                {
                    System.out.println("No has escrito una acción valida, para ver todas las acciones posibles usa 'help'");
                }

            }
        }

        System.out.println("Gracias por usar el cajero!");

    }
}
