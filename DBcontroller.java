import java.sql.*;

public class DBcontroller {
    private static String login_db_user = "backend";
    private static String login_db_password = "Admin1234";
    private static String login_db_server = "jdbc:mysql://localhost/mydb";
    Connection con;
    Statement state;

    void Connect()
    {
        try {  
            con = GetConnection();
        } catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    ResultSet ExecuteQuery(String SQL_query) throws SQLException
    {
        state = null;
        ResultSet results = null;
        try{
            state = con.createStatement();
            results = state.executeQuery(SQL_query);
            
            return results;
        }
        catch(SQLException ex)
        {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        finally{

            /*if (state != null) {
                try {
                    state.close();
                } catch (SQLException sqlEx) { } // ignore
        
                state = null;
            }*/

        }
        return results;
    }

    void CloseQuery(ResultSet res)
    {
        if (res != null) {
            try {
                res.close();
            } catch (SQLException sqlEx) { } // ignore
    
            res = null;
        }

        if (state != null) {
            try {
                state.close();
            } catch (SQLException sqlEx) { } // ignore
    
            state = null;
        }
    }

    void UpdateTable(String SQL_insert) throws SQLException
    {
        Statement state = con.createStatement();
        state.executeUpdate(SQL_insert);
        state.close();  
    }

    public static Connection GetConnection() throws SQLException
    {
        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        return (Connection) DriverManager.getConnection(login_db_server, login_db_user, login_db_password);
    }

    public void Disconnect() throws SQLException
    {
        con.close();
    }

}
