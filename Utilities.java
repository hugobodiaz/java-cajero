import java.io.*;

public abstract class Utilities {
    public static boolean isParsableInt(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
    public static boolean isParsableDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
    public static int readInt(Console cnsl, String errormsg)
    {
        String s = cnsl.readLine();

        while(!Utilities.isParsableInt(s))
        {
            System.out.println(errormsg);
            s = cnsl.readLine();
        }

        return Integer.parseInt(s);

    }

    public static Double readDouble(Console cnsl, String errormsg)
    {
        String s = cnsl.readLine();

        while(!Utilities.isParsableDouble(s))
        {
            System.out.println(errormsg);
            s = cnsl.readLine();
        }

        Double d=Double.parseDouble(s);

        return (double)Math.round(d*100)/100;

    }
}